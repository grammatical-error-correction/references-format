import subprocess
import argparse
import os

if __name__ == "__main__":
    # Define and parse program input
    parser = argparse.ArgumentParser(description="Keep source sentences from m2 file")
    parser.add_argument("-input", help="The path to an input m2 file.")
    parser.add_argument("-out", help="Output TXT filename.", required=True)
    args = parser.parse_args()
    # Run the main program
    cmd = [f"""grep ^S {args.input} | cut -c 3- > {args.out}"""]
    subprocess.check_output(cmd,shell=True)