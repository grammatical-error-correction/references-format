# GEC references format convertions

This BitBucket repository regroups scripts for converting different formats accepted by GEC standards in order to use specific metrics. It is mainly based on ERRANT, and BEA-19 scripts.

# Table of contents
- [Introduction](#introduction)
- [Venv](#venv)
- [From SGML to JSON](#from-sgml-to-json)
- [From JSON to M2](#from-json-to-m2)
- [From M2 to Parallel](#from-m2-to-parallel)
- [From M2 to XML](#from-m2-to-xml)
- [Extract source sentences from M2](#estract-source-sentences-from-m2)
- [References](#references)

# Introduction

In order to evaluate GEC models, manifold GEC metrics have been created, but they don't necessarily use the same input files. In our GitHub repository GEC metrics, we chose to select three of the most used and famous metrics for Grammatical Error Correction:
- MaxMatch (M2 scorer)
- I-measure
- GLEU

None of those metrics actually use the same inputs. Indeed,
- MaxMatch uses ERRANT filetype (see example in [examples/reference_m2](examples/reference_m2))
- I-measure uses `xml` filetype (see example in [example/reference.xml](examples/reference.xml))
- GLEU uses parallel file (see example in [examples/reference_parallel.txt](examples/reference_parallel.txt))

Also, some models, such as LanguageTool API, returns a JSON file (see example in [examples/reference.json]) which can be transformed into paralell files thanks to the double convertion JSOn -> M2, M2 -> Parallel.

# Venv 

Before diving into the different scripts that could be useful, please run the following executable bash script:

`chmod u+x venv.sh`
> to make it executable

`./venv.sh`
> to run the script

It will create a Python environment `errant_env` which contains the right installation of ERRANT toolkit.

Then, run this in your terminal to activate the environment:

`source errant_env/bin/activate`

> Thanks to that script, you will have no trouble installing ERRANT and SpaCy.

# From SGML to JSON

Be sure that the input SGML file respects the format. You just need to enter a path with SGLM files in it.

Example: `python3 src/sgml_to_json.py -input examples/ -out results/reference.json`

See documentation and additional features: `python3 scr/sgml_to_json.py -h`

# From JSON to M2

Be sure that the input JSON file respects the format.

Example: `python3 src/json_to_m2.py -input examples/reference.json -out results/reference_m2 -auto -docs`

See documentation and additional features: `python3 scr/json_to_m2.py -h`

> **Useful for**: MaxMatch

# From M2 to Parallel

Be sure that the input M2 file respects the format.

Example: `python3 src/m2_to_parallel.py -input examples/reference_m2 -out results/reference_parallel.txt`

See documentation and additional features: `python3 scr/m2_to_parallel.py -h`

> **Useful for**: GLEU

# From M2 to XML

Be sure that the input M2 file respects the format.

Example: `python3 src/m2_to_xml.py -input examples/reference_m2 -out results/reference.xml`

See documentation and additional features: `python3 scr/m2_to_xml.py -h`

> **Useful for**: I-measure

# Extract source sentences from M2 file

Be sure that the input JSON file respects the format.

Example: `python3 src/m2_to_source.py -input examples/reference_m2 -out results/source.txt`

See documentation: `python3 scr/m2_to_source.py -h`

# References

Code:

- [ERRor ANnotation Toolkit: Automatically extract and classify grammatical errors in parallel original and corrected sentences](https://github.com/chrisjbryant/errant)
- [Building Educational Applications 2019 Shared Task: Grammatical Error Correction](https://www.cl.cam.ac.uk/research/nl/bea2019st/)
- [Document-level Grammatical Error Correction](https://github.com/chrisjbryant/doc-gec)

Metrics:
- [GEC metrics: MaxMatch, I-measure, GLEU](https://bitbucket.org/grammatical-error-correction/metrics/src/master/)